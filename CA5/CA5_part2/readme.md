#### Assignment 5 : CA5

#### 1. Steps and commands - part II
* **Step1 :** Create folder CA5_part2:
  * mkdir /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA5/CA5_part2
* **Step2 :** Create file readme:
  * touch /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA5/CA5_part2/readme.md
* **Step3 :** Create issue #21 'CA5 part2' in Bitbucket
* **Step4 :** Add a test for the application
* **Step5 :** Create a Jenkinsfile:
  * Create the Jenkinsfile inside: CA2/CA2_part2/react-and-spring-data-rest-basic
* **Step6 :** Open jenkins:
  * Open up a terminal/command prompt window to the download directory (directory where is the jenkins.war)
  * Run the command: java -jar jenkins.war --httpPort=8091
  * Open http://localhost:8091 to access Jenkins
* **Step7 :** Install the HTML Publisher plugin, javadoc plugin and restart jenkins
* **Step8 :** Commit the changes addressing issue #21
  * git add .
  * git commit -m "Initial commit (addresses #21)"
  * git push
* **Step9 :** Create a pipeline:
  * Go to Jenkins -> Dashboard -> New Item -> Name: ca5_part2 -> Pipeline -> Ok
  * Pipeline -> Definition: Pipeline script from SCM
  * SCM: Git
  * Repository URL: https://bitbucket.org/mariana_pereira/devops-21-22-lmn-1211781
  * Credentials not needed because it's a public repository
  * Branch Specifier: */master
  * Script Path: CA2/CA2_part2/react-and-spring-data-rest-basic/Jenkinsfile
  * Press the 'Save' button and after 'Build now'
* **Step10 :** Create a new Dockerfile (from CA4_part2) and put it in folder: CA2/CA2_part2/react-and-spring-data-rest-basic (the same as Jenkinsfile)
* **Step11 :** Install the Docker Pipeline plugin and restart jenkins
* **Step12 :** Update the Jenkinsfile
* **Step13 :** Commit the changes (see step 8 for the commands)
* **Step14 :** Create credentials for Docker Hub in jenkins
* **Step15 :** Update Dockerfile and commit changes (see step 8 for the commands)
* **Step16 :** Update Jenkinsfile, test and commit the changes, resolving issue 21, and tag the commit with tag ca5-part2:
  * git add .
  * git commit -m "Assignment ca5-part2 (resolving #21)"
  * git push
  * git tag -a ca5-part2 -m "version ca5-part2"
  * git push origin ca5-part2
