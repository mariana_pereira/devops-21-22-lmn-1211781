#### Assignment 5 : CA5

#### 1. Steps and commands - part I
* **Step1 :** Create folder CA5:
  * mkdir /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA5
* **Step2 :** Create folder CA5_part1:
  * mkdir /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA5/CA5_part1
* **Step3 :** Create file readme:
  * touch /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA5/CA5_part1/readme.md
* **Step4 :** Install and configure Jenkins:
  * Follow the instructions for running directly the war file in (download the Generic Java Package (.war) LTS version): https://www.jenkins.io/doc/book/installing/war-file/
  * Open up a terminal/command prompt window to the download directory.
  * Run the command: java -jar jenkins.war -> did not worked
  * Run the command: java -jar jenkins.war --httpPort=8091 -> this time worked, the default port (8080) was being used
  * Open http://localhost:8091 to access Jenkins and insert the password that appeared in the terminal
  * Run the initial setup wizard with "install suggested plugins"
* **Step5 :** Create a Jenkinsfile:
  * Create the Jenkinsfile inside: CA2/CA2_part1/gradle_basic_demo
* **Step6 :** Create a pipeline:
  * Go to Jenkins -> Dashboard -> New Item -> Name: ca5_part1 -> Pipeline -> Ok
  * Pipeline -> Definition: Pipeline script from SCM
  * SCM: Git
  * Repository URL: https://bitbucket.org/mariana_pereira/devops-21-22-lmn-1211781
  * Credentials not needed because it's a public repository
  * Branch Specifier: */master
  * Script Path: CA2/CA2_part1/gradle_basic_demo/Jenkinsfile
  * Press the 'Save' button and after 'Build now' -> did not compile the project (forgot to commit changes)
* **Step7 :** Create issue #20 'CA5 part1' in Bitbucket
* **Step8 :** Commit the changes addressing issue #20
  * git add .
  * git commit -m "Initial commit (addresses #20)"
  * git push
* **Step8 :** Try to build the project again: Checkout stage ok but Assemble, Test and Archiving failed 
  * Run './gradlew clean' inside CA2/CA2_part1/gradle_basic_demo
  * Commit the changes (see step 8 for the commands) and force the push of gradle-wrapper.jar:
    * git add -f CA2/CA2_part1/gradle_basic_demo/gradle/wrapper/gradle-wrapper.jar
* **Step10 :** Go to Jenkins and build the project again -> this time everything went fine
* **Step11 :** Commit the changes, resolving the  issue 20, and tag the commit with tag ca5-part1
  * git add .
  * git commit -m "Assignment ca5-part1 (resolving #20)"
  * git push
  * git tag -a ca5-part1 -m "version ca5-part1"
  * git push origin ca5-part1 