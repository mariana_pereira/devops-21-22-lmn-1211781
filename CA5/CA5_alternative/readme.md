#### Assignment 5 : CA5

#### 1. Analyse an alternative (i.e., an alternative to Jenkins, in this case GitLab)
* **What is Jenkins?**
  * Jenkins is an automation server that is self-contained and open source. 
  * It allows the building, testing, and deployment for every release cycle of software. 
  * Through its fleet of plugins (over 1700) that seamlessly integrate with most CI/CD tools, Jenkins offers flexibility and covers almost all functional requirements.
  * Moreover, since Jenkins is written in Java, it is compatible with any system that has Java Runtime Environment (JRE) installed. 
  * This makes Jenkins widely usable due to its easy availability.
  * Jenkins is one of the most popular CI/CD automation servers in existence due to the following features:
    * **Easy Installation:** It is self-contained and offers complete installation packages for various operating systems such as Windows, Mac OS X, and Unix-based systems. 
      * To install Jenkins, all one needs is JRE installed in the system.
    * **Easy Configuration:** Configuring Jenkins is easy as it can be set up through its web interface. 
      * The web interface is very user-friendly, and configuration becomes easier with the built-in help offered by Jenkins.
    * **Open Source:** Jenkins is open source, so one can save on the hefty cost of license fees.
    * **Distribution:** Jenkins allows its users to distribute the workload across various machines located at different locations. 
      * With builds, tests, and deployments placed across multiple platforms, the process becomes faster as the system offers more productivity by working in parallel on different systems. 
      * This saves time and reduces the time frame of release cycles.
    * **Extensible:** Jenkins can be used to deliver over a fleet of functionalities due to strong plugin support. 
      * One can use Jenkin for projects as simple as those just requiring a CI server to complex projects involving CD.
    * **Plugins:** Jenkins offers comprehensive support for a wide range of plugins, where the user has the flexibility to choose a plugin that fits their business requirements. 
      * The massive number of plugins (1700+) are updated from time to time and offer every possible CI/CD functionality.
    * **Strong Community Support:** Jenkins has a strong and vibrant community of technical experts who support others working on Jenkins. 
      * This makes it easier to find solutions to possible issues through mutual cooperation and sharing ideas.

* **What is  GitLab?**
  * GitLab is a self-contained platform that supports the DevOps lifecycle through its web-based services. 
  * It manages the Git-repository through its Continuous Integration and Deployment pipelines, issue-tracking features, and wiki to store relevant files.
  * GitLab works on a freemium basis, i.e., it offers both free and paid services. 
  * It enables automation of the entire DevOps lifecycle, which involves planning, building, testing, deployment, and monitoring through release cycles.
  * GitLab enables Concurrent DevOps, which speeds up the development lifecycle. 
  * Through its services, GitLab combines Development, Security, and Ops to deliver faster with utmost security. 
  * It is written in Ruby, but the tech stack includes Go, Ruby on Rails, and Vue.js.
  * GitLab has gained a lot of traction in the CI/CD landscape due to the following features:
    * **Insights:** GitLab offers business insights that help manage the business aspects of the product. 
      * These insights allow users to keep track of changes implemented in the system and their implications on product performance. 
      * However, this feature is only available for paid versions.
    * **User Role Management:** It offers user role compliances to make the process more streamlined and secure. 
      * In addition to this, GitLab also provides user statistics that help in resource management and improve project management efficiency.
    * **Issues Tracking:** Tracking and assigning issues is easy with Gitlab through Task Lists, Thread Discussions, Labels, Milestones, Importing of issues from JIRA, and many more features, that allows effective resolution of issues through proactive, effortless tracking.
    * **Merge Management:** Collaborating and Version Control become seamless with GitLab due to easily implemented code merge requests and merge management systems done upon code review of branches.
    * **Plugins:** GitLab is well supported by several plugins that help make the different stages of DevOps a lot more efficient. 
      * These plugins can be used depending upon business requirements.
    * **Support:** GitLab offers Service Level Agreements (SLAs) and 24×5 Technical Support to paid users. 
      * They also have extensive and comprehensive documentation, covering a vast number of issues.
    * **Community:** It has a vibrant community forum that allows the users and technical experts from different parts of the world to connect, share their ideas, and collaborate for building better DevOps practices.
    
* * **Jenkins Vs. GitLab**
 
| Subject for comparison | Jenkins     | 	GitLab        |
|----------------------- |-------------|------------------|
| Language               |  Java       | Ruby               |
| Ease of Installation   |  Easy to Install       | Easy to Install               |
| Plugins                |  1700+ plugins available       | Limited Plugins               |
| Prerequisites               |  JRE should be installed  | Ruby, Go, Git, Node.js, and Redis should be installed |
| Operating Systems Supported |  Windows, Mac OS X, and Unix-like OS       | Supports only particular Unix-like OS such as Ubuntu, Debian, Red Hat Linux, Scientific Linux, Oracle Linux, CentOS, and OpenSUSE. It does not support Windows and macOS             |
| Open Source            |  Open Source and Free       | Open Source and Freemium               |
| Issue Tracking         |  Don’t have such functionality       | Offers various features for issue tracking and management             |
| Extensiveness	         |  Highly extensive as it can be used as a simple CI server or can be transformed into a complex CD system with the help of plugins      | Offers scalability to enhance the DevOps lifecycle for a project               |
| Support                |  Offers documentation and open source community support, but no technical support is provided as part of the SLA      | Provides 24×5 support for paid users and only self-support documents to free users as part of the SLA                |


#### 2. Implementation of the alternative (GitLab)
* **Step1 :** Go to www.gitlab.com and create an account 
* **Step2 :** Create a new project : Menu -> Projects -> Create new project -> Import project -> Repository by URL -> Set up CI/CD
* **Step3 :** GitLab generates a .gitlab-ci.yml file (it's possible to choose from a template) that was adapted for this assignment and added to my repository
* **Step4 :** The complete implementation of the alternative was not achieved, so it was never tested

 
