#### Assignment 3 : CA3

#### 1. Steps and commands - part I
* **Step1 :** Install VirtualBox for Windows
  * From: https://www.virtualbox.org/wiki/Downloads
* **Step2 :** Create folder CA3
  * mkdir /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA3
* **Step3 :** Create folder CA3_part1:
  * mkdir /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA3/CA3_part1
* **Step4 :** Create file readme:
  * touch /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA3/CA3_part1/readme.md
* **Step5 :** Create a Virtual Machine (VM):
  * Click on "New" button in the start menu
  * Name the VM : ubuntu-devops
  * Select type (Linux) and version (Ubuntu (64bit))
  * Select memory (RAM) size (2048 MB)
  * Create virtual hard disk with file type VDI (Virtual Disk Image), dynamically allocated storage, and 10 GB of space
* **Step6 :** Download image file disk (mini.iso) to install the OS in the VM from: https://help.ubuntu.com/community/Installation/MinimalCD (Ubuntu 18.04 "Bionic Beaver")
* **Step7 :** Install the OS in the VM:
  * In the VirtualBox menu, with the new VM (created in step 5) selected, click on settings
  * Click on Storage and select the image file disk downloaded in step 6 and select the "Live CD/DVD" option
  * Start the VM to install the OS 
  * Configure the OS during installation process
* **Step8 :** Remove the mini.iso file from the VM (after shutting down the VM)
* **Step9 :** Change VM settings:
  * Set Network Adapter 1 as Nat (done by default)
  * Set Network Adapter 2 as Host-only Adapter:
    * Click on Settings, Network, Adapter 2, Enable Network Adapter, select Host-only Adapter and select the default (that already existed)
  * Check the IP address range of this network, in this case it is: 192.168.56.1/24
* **Step10 :** After starting the VM, log on into the VM and continue the setup:
  * Update the packages repositories:
    * sudo apt update
  * Install the network tools:
    * sudo apt install net-tools
  * Edit the network configuration file to setup the IP:
    * sudo nano /etc/netplan/01-netcfg.yaml
    * Change the file to:
      * network:
        * version: 2
        * renderer: networkd
          * ethernets:
            * enp0s3:
              * dhcp4: yes
            * enp0s8:
              * addresses:
                * - 192.168.56.5/24
  * Apply the new changes:
    * sudo netplan apply
  * Install openssh-server so that we can use ssh to open secure terminal sessions to the VM (from other hosts):
    * sudo apt install openssh-server
  * Enable password authentication for ssh:
    * sudo nano /etc/ssh/sshd_config
    * uncomment the line PasswordAuthentication yes
    * sudo service ssh restart
  * Install a ftp server so that we can use the FTP protocol to transfers files to/from the VM (from other hosts):
    * sudo apt install vsftpd
  * Enable write access for vsftpd:
    * sudo nano /etc/vsftpd.conf
    * uncomment the line write_enable=YES
    * sudo service vsftpd restart
* **Step11 :** In the host (Windows), in a terminal/console, typed:
  * ssh mariana@192.168.56.5
  * with this command the host has a remote connection to the VM 
* **Step12 :** Install git and java (this time from the ssh remote connection):
  * sudo apt install git
  * sudo apt install openjdk-11-jdk-headless
* **Step13 :** Download and install FileZilla (client), to transfer files to/from the VM from: https://filezilla-project.org
* **Step14 :** Clone my individual repository inside the VM:
  * Create a new app password in bitbucket (it will be needed for the next command)
  * From the terminal used in step 12: git clone https://Mariana_Pereira@bitbucket.org/mariana_pereira/devops-21-22-lmn-1211781.git
* **Step15 :** Try to build and execute the spring boot tutorial basic project (from the CA1):
  * Go to the root of the basic project: cd devops-21-22-lmn-1211781/basic
  * chmod u+x mvnw (to give permissions to execute the application)
  * ./mvnw spring-boot:run (to build and run the app in the VM)
  * Open the application through the browser in my host computer in the following url: http://192.168.56.5:8080/
  * The page shows what it is supposed to
* **Step16 :** Try to build and execute the gradle_basic_demo project (from the CA2-PART1):
  * Go to the root of the gradle_basic_demo project: cd .. ; cd CA2/CA2_part1/gradle_basic_demo
  * chmod u+x gradlew (to give permissions to execute the application)
  * ./gradlew build (to build and run the app in the VM) - did not work, missing gradle-wrapper.jar in the VM
  * With FileZilla put the gradle-wrapper.jar in the correct folder in the VM
  * ./gradlew build (this time worked fine)
  * Change the build.gradle file: in task runClient change localhost to 192.168.56.5 (the IP of the VM)
  * With FileZilla put the build.gradle file in the correct folder in the VM
  * Try to run the executeServer task: ./gradlew executeServer (works fine)
  * Try to run the runClient task: ./gradlew runClient (inside the host machine and not the VM - see step 17 for the reason)
  * The chat app works fine
* **Step17 :** Because the VM doesn't have a graphical environment isn't possible to execute the client side
* of the chat app inside the VM (the client has a graphic interface but the VM does not)
* **Step18 :**  Try to build and execute the spring boot tutorial basic project using gradle instead of maven (from the CA2-PART2):
  * Go to the root of the project
  * chmod u+x gradlew (to give permissions)
  * ./gradlew build (to build - worked fine)
  * ./gradlew bootRun (to run the app)
  * In the host machine go to the url: http://192.168.56.5:8080/ (works fine - the page shows what is supposed to)
* **Step19 :** Create issue in Bitbucket to execute the assignment CA3-PART1
* **Step20 :** Commit the changes, resolving the previous issue, and tag the commit with tag ca3-part1
  * git add .
  * git commit -m "Assignment ca3-part1 (resolving #14)"
  * git push
  * git tag -a ca3-part1 -m "version ca3-part1"
  * git push origin ca3-part1
