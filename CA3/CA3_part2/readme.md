#### Assignment 3 : CA3

#### 1. Steps and commands - part II
* **Step1 :** Create folder CA3_part1:
  * mkdir /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA3/CA3_part2
* **Step2 :** Create file readme:
  * touch /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA3/CA3_part2/readme.md
* **Step3 :** Download and install the Vagrant from:
  * https://www.vagrantup.com/downloads.html
* **Step4 :** Check if everything is ok:
  * vagrant -v (to see the version)
* **Step5 :** Copy the Vagrantfile (from: https://bitbucket.org/atb/vagrant-multi-spring-tut-demo) to the repository (inside the folder CA3_part2)
* **Step6 :** Create issue in Bitbucket to update the Vagrantfile configuration (#15)
* **Step7 :** Make my repository public before running the Vagrantfile
* **Step8 :** Update the Vagrantfile to use the react-and-spring-data-rest-basic (from CA2 part2):
  * Change sudo apt-get install openjdk-8-jdk-headless -y to sudo apt-get install openjdk-11-jdk-headless -y (to install java 11 and not 8)
  * Change the app to be cloned (lines 70 and 71 of the Vagrantfile)
* **Step9 :** In the folder where the Vagrantfile is:
  * vagrant up
* **Step10 :** The previous command generated the folder .vagrant
  * In the .gitignore add : .vagrant and *.war (to prevent git from tracking this type of files/directories - they won't be committed to remote)
* **Step11 :** The VM weren't created (the box does not support java 11)
  * vagrant halt (to stop vagrant)
  * vagrant destroy (to destroy the VM's)
  * vagrant status (to verify that the VM was destroyed)
* **Step12 :** The ubuntu version does not support java 11 - chose a different box from:
  * https://app.vagrantup.com/boxes/search?provider=virtualbox
  * Update the Vagrantfile (line 6, 22 and 51)
  * vagrant destroy
  * vagrant up
* **Step13 :** Error: web: cp: cannot stat './build/libs/basic-0.0.1-SNAPSHOT.war': No such file or directory
  * In the root of the project (devops-21-22-lmn-1211781/CA2/CA2_part2/react-and-spring-data-rest-basic): ./gradlew build
  * Update Vagrantfile: in line 82 change the name of the file 
* **Step14 :** Commit the changes:
  * git add .
  * git commit -m "Update the Vagrantfile (addresses #15)"
  * git push
* **Step15 :**  Update the app so that the spring application uses the H2 server in the db VM:
  * Update the build.gradle file (from CA2_part2):
    * In plugins add: id 'war' (line 6)
    * In dependencies update line 22 and add lines from 22 to 26:
      * dependencies {
        * implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
        * implementation 'org.springframework.boot:spring-boot-starter-data-rest'
        * implementation 'org.springframework.boot:spring-boot-starter-thymeleaf'
        * runtimeOnly 'com.h2database:h2'
        * testImplementation ('org.springframework.boot:spring-boot-starter-test') {
          * exclude group: 'org.junit.vintage', module: 'junit-vintage-engine'
        * }
        * // To support war file for deploying to tomcat
        * providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
      * }
    * Update frontend tp:
    * frontend {
      * nodeVersion = "14.17.3"
      * assembleScript = "run webpack"
    * }
  * Update the package.json file (from CA2_part2):
    * Update the scripts section to:
      * "scripts": {
      * "watch": "webpack --watch -d",
      * "webpack": "webpack"
      * },
  * Create file react-and-spring-data-rest-basic/src/main/java/com/greglturnquist/payroll/ServletInitializer.java (in CA2_part2)
  * Update the application.properties file (from CA2_part2: react-and-spring-data-rest-basic/src/main/resources/application.properties)
  * Update the app.js file (from CA2_part2: react-and-spring-data-rest-basic/src/main/js/app.js):
    * Update the path in componentDidMount() method from '/api/employees' to '/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/api/employees'
  * Update the index.html file (from CA2_part2: react-and-spring-data-rest-basic/src/main/resources/templates/index.html)
    * in line 6 removed the '/' before main.css
* **Step16 :** Delete the .jar files so when the command ./gradlew clean built runs, it will create the new .war files required.
* **Step17 :** In the project root (devops-21-22-lmn-1211781/CA2/CA2_part2/react-and-spring-data-rest-basic) run:
  * ./gradlew clean build
* **Step18 :** Commit the changes (see step 14 for the commands)
* **Step19 :** Execute the command: vagrant up --provision -> to update the VM's
* **Step20:** Check if it’s working:
  * Go to: http://192.168.56.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/ (or http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/)
  * The H2 console can be accessed from:
    * http://192.168.56.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console (or http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console)
    * Note: On JDBC URL type: jdbc:h2:tcp://192.168.56.11:9092/./jpadb
    * From the H2 console is possible to execute the SQL commands, for example, to add data in the DB that will be shown in the page
* **Step21 :** Commit the changes, resolving issue #16, and tag the commit with tag ca3-part2:
  * git add .
  * git commit -m "Assignment ca3-part2 (resolving #16)"
  * git push
  * git tag -a ca3-part2 -m "version ca3-part2"
  * git push origin ca3-part2
