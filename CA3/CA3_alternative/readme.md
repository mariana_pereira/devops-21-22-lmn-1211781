#### Assignment 3 : CA3

#### 1. Analyse an alternative (in this case VMWare)
* **What is VirtualBox?**
  * VirtualBox is Oracle’s x86 and AMD64/Intel64 virtualization software. It is a free, open-source
  * virtualization product, distributed under the GNU General Public License (GPL) version 2.
  * The software allows you to run virtual machines on your host operating system. Additionally, it lets you
  * establish a managed connection between the VMs and even the host if needed.
  * You can run VirtualBox on Linux, Windows, Mac OS, and Oracle Solaris.
  * In 2010, Oracle introduced the VirtualBox Extension Pack, a closed-source complemental package with
  * additional features. It included features such as support for USB2/USB3 and RDP.
* **What is VMware?**
  * VMware has a number of virtualization products. VMware Workstation Player is free x64 virtualization
  * software available for non-commercial use. The company underlines that the free version is for students
  * and educators. If you want to use the Player for commercial use, you need to pay for the Workstation
  * Player commercial license.
  * It is used for managing and creating virtual machines but works best when running a single VM. You can
  * install the free virtualization software if you have a Linux or Windows OS host.
  
* * **VirtualBox Vs. VMware**

| Subject for comparison | VirtualBox   | 	VMware |
|------------------- |-------------|------------------|
| Software Virtualization |  yes | no             |
| Hardware Virtualization |   yes            | yes                  |
| Host Operating Systems |   Mutable through rollback, cherry-pick, rebase             | Immutable beyond rollback                |
| Guest Operating Systems |   Each revision unique by calculated SHA-1              | Incremental, numerical index of revision (0, 1, 2, etc)                  |
| User Interface |   Supported via revert command; arbitrary via rebase and cherry-pick              | Supported via backout, revert commands                  |
| Snapshots |    	Yes     | Snapshots only supported on paid virtualization products, not on VMware Workstation Player              |
| Virtual Disk Format | VDI, VMDK, VHD, HDD | 	VMDK   |
| Virtual Disk Allocation Type |   Preallocated: fixed disks;   Dynamically allocated: dynamically allocated disks;       | Preallocated: provisioned disks;   Dynamically allocated: thin provisioned disks;  |
| Virtual Network Models |   	Not attached, NAT, NAT Network, Bridged adapter, Internal network, Host-only adapter, Generic (UDP, VDE)  | NAT, Bridged, Host-only + Virtual network editor (on VMware workstation and Fusion Pro)            |
| USB Devices Support |  	USB 2.0/3.0 support requires the Extension Pack (free) |   Out of the box USB device support    |
| 3D Graphics | 	Up to OpenGL 3.0 and Direct3D 9;   Max of 128 MB of video memory; 3D acceleration enabled manually  |  Up to OpenGL 3.3, DirectX 10;   Max of 2GB of video memory; 3D acceleration enabled by default              |
| Integrations |   	VMDK, Microsoft’s VHD, HDD, QED, Vagrant, Docker   | Requires additional conversion utility for more VM types;   VMware VSphere and Cloud Air (on VMware Workstation)            |
| VirtualBox Guest Additions vs. VMware Tools |  Installed with the VBoxGuestAdditions.iso file | Install with a .iso file used for the given VM (linux.iso, windows.iso, etc.)               |
| API for Developers |   	API and SDK | 	Different APIs and SDKs   |
| Cost and Licenses |   Free, under the GNU General Public License   | VMware Workstation Player is free, while other VMware products require a paid license             |



#### 2. Implementation of the alternative (VMWare)
* **Step1 :** Download and install VMWare:
  * From: https://customerconnect.vmware.com/en/downloads/details?downloadGroup=WKST-PLAYER-1623-NEW&productId=1039&rPId=85399
* **Step2 :** Install VMWare Utility:
  * From: https://www.vagrantup.com/docs/providers/vmware/installation
* **Step3 :** Install the Vagrant VMware provider plugin using the standard plugin installation procedure:
  * In the folder (devops-21-22-lmn-1211784/CA3/CA3_alternative) run: vagrant plugin install vagrant-vmware-desktop
* **Step4 :** Create a new Vagrant configuration file for the project:
  * In the folder (devops-21-22-lmn-1211784/CA3/CA3_alternative) run: vagrant init vmware
* **Step5 :** Verify the network created by VMWare:
  * In Network Connections -> VMWare Network Adapter VMnet1 -> double click -> Details -> IPv4 Address
  * The network is: 192.168.200.0/24 (192.168.200.1 with mask: 255.255.255.0)
* **Step6 :** Update the Vagrantfile (generated in step 4) based on the Vagrantfile used in CA3_part2:
  * Selected IP's for the VM's:
    * web: 192.168.200.10
    * db: 192.168.200.11
* **Step7 :** Update CA2_part2/react-and-spring-data-rest-basic/src/main/resources/application.properties IP address to the VM db IP:
  * The IP part used in CA2_part2 is now commented (line 6) and a new line (7) with the new IP was added.
* **Step8 :** Commit the changes
* **Step9 :** In folder /devops-21-22-lmn-1211781/CA3/CA3_alternative run:
  * vagrant up --provider vmware_desktop
* **Step10 :** Check if it’s working:
  * Go to: http://192.168.200.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/ (or http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/)
  * The H2 console can be accessed from:
    * http://192.168.200.11:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console (or http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console)
    * Note: On JDBC URL type: jdbc:h2:tcp://192.168.56.11:9092/./jpadb
    * From the H2 console is possible to execute the SQL commands, for example, to add data in the DB that will be shown in the page
* **Step11 :** Commit the changes and tag the commit with tag ca3-alternative:
