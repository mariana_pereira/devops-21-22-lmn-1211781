#### Assignment 2 : CA2

#### 1. Steps and commands - part I
* **Step1 :** Create folder CA2:
    * mkdir /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA2
* **Step2 :** Create folder CA2_part1:
    * mkdir /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA2/CA2_part1
* **Step3 :** Create file readme:
    * touch /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA2/readme.md
* **Step4 :** Create issue #5 to download and commit to my repository the example application (directly in Bitbucket)
* **Step5 :** Clone the example application to the folder CA2_part1:
    * git clone https://bitbucket.org/luisnogueira/gradle_basic_demo.git
* **Step6 :** Delete the .git folder (and it's contents) and .gitignore file from folder CA2/CA2_part1/gradle_basic_demo
    * rm -rf /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA2/CA2_part1/gradle_basic_demo/.git
    * rm /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA2/CA2_part1/gradle_basic_demo/.gitignore
* **Step7 :** Commit the changes to remote, resolving issue #5:
    * git add .
    * git commit -m "Initial commit (resolving #5)"
    * git push
* **Step8 :** Run demo application that implements a basic multithreaded chat room server:
    * In the root of the project (C:\WsSwitchISEP\devops-21-22-lmn-1211781\CA2\CA2_part1\gradle_basic_demo):
      * Open terminal and insert command: ./gradlew build (to build a .jar file with the application)
      * Run the command: java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001 (to run the server)
      * On another  terminal run: ./gradlew runClient (to run client 1)
      * On another terminal run: ./gradlew runClient (to run client 2)
    * Test the app by sending messages from client 1 to client 2 and vice versa.
    * Close the app.
* **Step9 :** Create issue #6 to add a new task to execute the server (directly in Bitbucket)
* **Step10 :** In the build.gradle file create a new task named executeServer:
  * task executeServer(type:JavaExec, dependsOn: classes){
  *  group = "DevOps"
  *  description = "Launches the server on 59001 "
  *  classpath = sourceSets.main.runtimeClasspath
  *  mainClass = 'basic_demo.ChatServerApp'
  *  args '59001'
  *  }
* **Step11 :** Run the new task to test it:
  * In the root of the project open a terminal and run the command:  ./gradlew executeServer
  * In the same folder open another terminal and run the command: ./gradlew runClient (client 1)
  * In the same folder open another terminal and run the command: ./gradlew runClient (client 2)
  * Test the app by sending messages from client 1 to client 2 and vice versa.
  * Close the app.
* **Step12 :** Commit the changes to remote, resolving issue #6 (see step 7 for the commands)
  * This time the commit message was: "Add a new task to execute the server (resolving #6)"
* **Step13 :** Create issue #7 to add a simple unit test and update the gradle script so that it is able to execute the test (directly in Bitbucket)
* **Step14 :** Add the junit 4.12 dependency in gradle:
  * In file build.gradle inside the dependencies {} insert: testImplementation('junit:junit:4.12')
  * Run the command to build the project: ./gradlew build
* **Step15 :** Create the test class: AppTest.java and copy the given code for the unit test into that class. 
* **Step16 :** Build the project to ensure it's ok: 
  * ./gradlew build (to build the project)
* **Step17 :** Run the test: 
  * ./gradlew test
* **Step18 :** See the tasks (this step wasn't mandatory):
  * ./gradlew tasks
* **Step19 :** Commit the changes to remote, resolving issue #7 (see step 7 for the commands)
* **Step20 :** Create issue #8 to add a new task of type Copy to be used to make a backup of the sources of the application (directly in Bitbucket)
* **Step21 :** In the build.gradle file create a new task named copySrcFolder:
  * This task should copy the contents of the src folder to a new backup folder
  * task copySrcFolder(type: Copy) {
  * from 'src'
  * into 'backup'
  * }
* **Step22 :** Run the new task to test it:
  * In the root of the project open a terminal and run the command:  ./gradlew copySrcFolder
* **Step23 :** Commit the changes to remote, resolving issue #8 (see step 7 for the commands)
  * This time the commit message was: "Add a new task to make a backup of the sources of the application (resolving #8)"
* **Step24 :** Create issue #9 to add a new task of type Zip to be used to make an archive (i.e., zip file) of the sources of the application (directly in Bitbucket)
* **Step25 :** In the build.gradle file create a new task named makeZipArchiveSrc:
  * It should copy the contents of the src folder to a new zip file.
  * task makeZipArchiveSrc(type: Zip) {
  *  archiveFileName  = 'src.zip'
  *  from 'src'
  *  destinationDirectory = file('backup')
  *  }
* **Step26 :** Run the new task to test it:
  * In the root of the project open a terminal and run the command:  ./gradlew makeZipArchiveSrc
* **Step27 :** Commit the changes to remote, resolving issue #9 (see step 7 for the commands)
  * This time the commit message was: "Add a new task to make an archive of the sources of the application (resolving #9)"
* **Step28 :** Modified the readme.md file, commit changes and tag the commit with the tag ca2-part1:
  * git tag -a ca2-part1 -m "version ca2-part1"
  * git push origin ca2-part1

#### 2. Steps and commands - part II
* **Step1 :** Create folder CA2_part2:
  * mkdir /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA2/CA2_part2
* **Step2 :** Create branch named tut-basic-gradle:
  * To create the branch "tut-basic-gradle": git branch tut-basic-gradle
  * To see all the branches (local and remote):  git branch -a
  * To work on the new branch: git checkout tut-basic-gradle
  * To push the new branch to remote repository and, with the option -u, set the default remote branch for the current local branch: git push -u origin tut-basic-gradle
* **Step3 :** Use https://start.spring.io to start a new gradle spring boot project with the following dependencies: Rest Repositories; Thymeleaf; JPA; H2
* **Step4 :** Extract the generated zip file inside the folder "CA2/CA2_part2/"
* **Step5 :** Run the command ./gradlew tasks in the root of the project (/c/WsSwitchISEP/devops-21-22-lmn-1211781/CA2/CA2_part2/react-and-spring-data-rest-basic) to check the available gradle tasks 
* **Step6 :** Delete src file in the project root
  * rm -rf src
* **Step7 :** Copy the src folder (and all its subfolders) from the basic folder of the tutorial into this new folder:
  * cp -R C/tut-react-and-spring-data-rest-main/basic/src CA2/CA2_part2/react-and-spring-data-rest-basic/
* **Step8 :** Copy the files webpack.config.js and package.json:
  * cp -R C/tut-react-and-spring-data-rest-main/basic/webpack.config.js CA2/CA2_part2/react-and-spring-data-rest-basic/
  * cp -R C/tut-react-and-spring-data-rest-main/basic/package.json CA2/CA2_part2/react-and-spring-data-rest-basic/
* **Step9 :** Delete the folder src/main/resources/static/built/ since this folder should be generated from the javascrit by the webpack tool
  * rm -rf built (from the parent folder)
* **Step10 :** Run the application :
  * ./gradlew bootRun (the page http://localhost:8080/ is empty as expected)
* **Step11 :** The command ./gradlew bootRun shows error message:  Process 'command 'C:\Program Files\Java\jdk-11.0.13\bin\java.exe'' finished with non-zero exit value 1
  * Redo steps 3 to 10. After this, it worked. (Before tried ./gradlew clean , ./gradlew build and ./gradlew bootRun, but it did not work)
* **Step12 :** Add the gradle plugin org.siouan.frontend to the project so that gradle is also able to manage the frontend:
  * In the  build.gradle file, in the plugins block, add the line: id 'org.siouan.frontend-jdk11' version '6.0.0'
* **Step13 :** Add the following code in build.gradle to configure the previous plugin: 
  * frontend {
  *  nodeVersion = "14.17.3"
  *  assembleScript = "run build"
  *  cleanScript = "run clean"
  *  checkScript = "run check"
  *  }
* **Step14 :**  Update the scripts section/object in package.json to configure the execution of webpack:
  * "scripts": {
  *  "webpack": "webpack",
  *  "build": "npm run webpack",
  *  "check": "echo Checking frontend",
  *  "clean": "echo Cleaning frontend",
  *  "lint": "echo Linting frontend",
  *  "test": "echo Testing frontend"
  *  },
* **Step15 :** Run the ./gradlew build command in the project root.
* **Step16 :** Execute the application by using ./gradlew bootRun (now the page is not empty like it was after step 10)
* **Step17 :** Create issue in Bitbucket to add a task to gradle to copy the generated jar to a folder named "dist" located at the project root folder level
* **Step18 :** In the build.gradle file create a new task named copyJarToDist:
  * task copyJarToDist(type: Copy) {
  *  from('build/libs') {
  *  include '**/*.jar'
  *  }
  *  into 'dist'
  *  }
* **Step19 :** Run the new task to test it:
  * In the root of the project open a terminal and run the command:  ./gradlew copyJarToDist
* **Step20 :** Commit the changes to remote, resolving issue #10:
  * git add .
  * git commit -m "Add task to copy the generated jar (resolving #10)"
  * git push
* **Step21 :** Create issue in Bitbucket to add a task to gradle to delete all the files generated by webpack (usually located at src/resources/main/static/built/). This new task should be executed automatically by gradle before the task clean
* **Step22 :** In the build.gradle file create a new task:
  * clean.doFirst {
  *  delete 'src/main/resources/static/built'
  *  }
* **Step23 :** Run the new task to test it (this time I will run the clean task to see if the other is executed before):
  * In the root of the project open a terminal and run the command:  ./gradlew clean
* **Step24 :** Commit the changes to remote, resolving issue #11 (see step 20 for the commands)
* **Step25 :** Merge the branch tut-basic-gradle into the Master branch:
  * git checkout master
  * git merge tut-basic-gradle
  * git push

#### 3. Analyse an alternative technological solution for the build automation tool (i.e., not based on Gradle)
* **Selected alternative: Maven**

* **What is Gradle?** 
  * Gradle is a build automation system that is fully open source and uses the concepts you see on Apache Maven and Apache Ant.
  * It uses domain-specific language based on the programming language Groovy, differentiating it from Apache Maven, which uses XML for its project configuration. It also determines the order of tasks run by using a directed acyclic graph.
  * Several developers created Gradle and first released in 2007, and in 2013, it was adopted by Google as the build system for Android projects.
  * It was designed to support multi-project builds that are expected to be quite huge. It also allows for incrementally adding to your build, because it knows which parts of your project are updated.
  * Tasks that are dependent on updated parts are no longer re-executed.
  * For now, the latest stable release is version 3.4, which was launched in February 2017.
  * It supports development and subsequent deployment using Java, Scala, and Groovy, with other project workflows and languages being introduced in the future.

* **What is Maven?**
  * Maven is used for project build automation using Java.
  * It helps you map out how a particular software is built, as well as its different dependencies.
  * It uses an XML file to describe the project that you are building, the dependencies of the software with regards to third-party modules and parts, the build order, as well as the needed plugins.
  * There are pre-defined targets for tasks such as packaging and compiling.
  * Maven will download libraries and plugins from the different repositories and then puts them all in a cache on your local machine.
  * While predominantly used for Java projects, you can use it for Scala, Ruby, and C#, as well as a host of other languages.

* **Gradle vs Maven**
  * There are some fundamental differences in the way that the two systems approach builds. Gradle is based on a graph of task dependencies – in which tasks are the things that do the work – while Maven is based on a fixed and linear model of phases.
  * With Maven, goals are attached to project phases, and goals serve a similar function to Gradle’s tasks, being the “things that do the work.”
  * Performance-wise, both allow for multi-module builds to run in parallel.
  * However, Gradle allows for incremental builds because it checks which tasks are updated or not.
  * If it is, then the task is not executed, giving you a much shorter build time.
  * Other distinguishing performance features you can find on Gradle include:
    * Incremental compilations for Java classes
    * Compile avoidance for Java
    * The use of APIs for incremental subtasks
    * A compiler daemon that also makes compiling a lot faster
  * When it comes to managing dependencies, both Gradle and Maven can handle dynamic and transitive dependencies, to use third-party dependency caches, and to read POM metadata format.
  * You can also declare library versions via central versioning definition and enforce central versioning.
  * Both download transitive dependencies from their artifact repositories.
  * Maven has Maven Central while Gradle has JCenter, and you can define your own private company repository as well.
  * If there are several dependencies required, Maven can download these simultaneously.
  * Gradle, however, wins when it comes to API and implementation dependencies, as well as inherently allowing concurrent safe caches.
  * It also keeps repository metadata along with cached dependencies, ensuring that two or more projects using the same cache will not overwrite each other, and it has a checksum-based cache and can synchronize cache with the repository.
  * Furthermore, Gradle is compatible with IVY Metadata, allowing you to define custom rules to specify a version for a dynamic dependency, and resolving version conflicts.
  * These are not available on Maven.
  * Other dependency management features that you can find only on Gradle are:
    * The use of substitution rules for compatible libraries
    * The use of ReplacedBy rules
    * Better metadata resolution
    * The ability to dynamically replace project dependencies with external ones, and vice versa
  * Gradle also gives you an easier time when working with composite builds, and it enables you to work with ad-hoc and permanent composite builds, as well as combine different builds and importing a composite build into Eclipse of IntelliJ IDEA.
  * As far as execution models are concerned, both have task groups and descriptions.
  * Both enable you to build only the specified project and its dependencies.
  * Gradle, however, has a fully configurable DAG, while with Maven, a goal can be attached only to one other goal. Multiple goals take on the form of an ordered list.
  * Gradle also allows task exclusions, transitive exclusions, and task dependency inference.
  * Gradle also has advanced features for task ordering and finalizers, among others.
  * Administering build infrastructure is another strong point for Gradle as it uses wrappers that accept auto provisioning, while with Maven, you need to have an extension to support self-provisioning builds.
  * Gradle also enables you to configure version-based build environments without having to set these up manually.
  * It also allows custom distributions.
  
#### 4. Implementation of the alternative (Maven)
* **Step1 :** Create folder CA2_part2_alternative:
  * mkdir /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA2/CA2_part2_alternative
* **Step2 :** Use https://start.spring.io to start a new maven spring boot project with the following dependencies: Rest Repositories; Thymeleaf; JPA; H2
* **Step3 :** Extract the generated zip file inside the folder "CA2/CA2_part2_alternative/"
* **Step4 :** Delete the src folder
* **Step5 :** Copy the src folder from the tut-react-and-spring-data-rest application into react-and-spring-data-rest-basic folder
* **Step6 :** Copy the package.json and webpack.config.js files into the previous mentioned folder
* **Step7 :** Delete the folder src/main/resources/static/built
* **Step8 :** Run the application:
  * ./mvnw spring-boot:run (the page http://localhost:8080/ is empty as expected)
* **Step9 :** Add the frontend-maven-plugin to the project so that maven is also able to manage the frontend
* (from https://github.com/eirslett/frontend-maven-plugin).
  * See the pom.xml file, from line 51 to 88 (to see the changes required to use the plugin)
* **Step10 :** Update the scripts section/object in package.json to configure the execution of webpack (see line 27 to 34 in the file)
* **Step11 :** Run the ./mvnw install command in the project root.
* **Step12 :** Execute the application by using ./mvnw springboot:run (now the page http://localhost:8080/ is not empty)
* **Step13 :** Commit the changes:
  * git add .
  * git commit -m "Maven project"
  * git push
* **Step14 :** Create an issue in Bitbucket to add a task to copy the generated jar to a folder named "dist" 
* **Step15 :** Add a task to copy the generated jar to a folder named "dist" located at the project root folder level
  * See the pom.xml file, from line 89 to 113 (to see the task)
* **Step16 :** Build the project to test it:
  * ./mvnw install
* **Step17 :** Commit the changes to remote, resolving issue #12 (see step 13 for the commands)
* **Step18 :** Create an issue in Bitbucket to add a task to maven to delete all the files generated by webpack
* This new task should be executed automatically before the task clean
* **Step19 :** Add a task to delete all the files generated by webpack:
  * See the pom.xml file, from line 114 to 125 (to see the task)
* **Step20 :**  Test the task:
  * ./mvnw clean (to see if occurs before the clean task)
* **Step21 :** Commit the changes to remote, resolving issue #13 (see step 13 for the commands)
* **Step22 :** Update the readme.md file and commit the changes (see step 13 for the commands)
* **Step23 :** Tag the commit with tag "ca2-part2":
  * git tag -a ca2-part2 -m "version ca2-part2"
  * git push origin ca2-part2



