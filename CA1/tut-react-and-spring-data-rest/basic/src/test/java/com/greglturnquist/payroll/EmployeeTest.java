package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void createsValidEmployee() {
        Employee emp1 = new Employee("Name1","Last1","teste1",1,"test@email.com");
    }
    @Test
    void createsInvalidEmployee0jobYears() {
        Exception exception = assertThrows(Exception.class, () -> {
            Employee emp1 = new Employee("Name1","Last1","teste1",0,"test");
        });

        String expectedMessage = "not valid parameter";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }
    @Test
    void createsInvalidEmployeeEmptyName() {
        Exception exception = assertThrows(Exception.class, () -> {
            Employee emp1 = new Employee("","Last1","teste1",1,"test");
        });

        String expectedMessage = "not valid parameter";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }
    @Test
    void createsInvalidEmployeeEmptyLastName() {
        Exception exception = assertThrows(Exception.class, () -> {
            Employee emp1 = new Employee("Name1","","teste1",1,"test");
        });

        String expectedMessage = "not valid parameter";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }
    @Test
    void createsInvalidEmployeeEmptyDescription() {
        Exception exception = assertThrows(Exception.class, () -> {
            Employee emp1 = new Employee("Name1","Last1","",1,"test");
        });

        String expectedMessage = "not valid parameter";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }
}