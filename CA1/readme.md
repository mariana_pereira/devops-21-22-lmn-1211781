#### Assignment 1 : CA1 

#### 1. Steps and commands - part I
* **Step1 :** Create folder CA1:  
  * mkdir /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA1
* **Step2 :** Create file readme:  
  * touch /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA1/readme.md 
* **Step3 :** Copy Tutorial React.js and Spring Data REST Application into folder named CA1: 
  * cp -r /c/WsSwitchISEP/ut-react-and-spring-data-rest /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA1
* **Step4 :** Send changes to remote repo:  
  * git add . 
  * git commit -m "First commit for CA1" 
  * git push
* **Step5 :** Tag the last commit: 
  * git tag -a v1.1.0 -m "version 1.1.0" 
  * git push origin v1.1.0
* **Step6 :** Create issue #1 to implement new feature (directly in Bitbucket)
* **Step7 :** Implement the new feature, commit changes and resolve issue #1: 
  * git status 
  * git add . 
  * git commit -m "Added new field jobYears (resolving #1)" 
  * git push
* **Step8 :** Tag the commit: 
  * git tag -a v1.1.1 -m "version 1.1.1" 
  * git push origin v1.1.1
* **Step9 :** Code doesn't compile in command line: 
  * right-click on the pom.xml file in the root directory, and select 'Add as maven project'
* **Step10 :** Create issue #2 to test the new feature (directly in Bitbucket)
* **Step11 :** Test the new feature, commit changes and resolve issue #2: 
  * git status 
  * git add . 
  * git commit -m "Added tests for Employee (resolving #2)" 
  * git push
* **Step12 :** Tag the commit: 
  * git tag -a v1.1.2 -m "version 1.1.2" 
  * git push origin v1.1.2
* **Step13 :** Some refactoring of the code and commit changes with tag v1.2.0: 
  * See step 11 for the commands to commit changes. The commit message for this commit is "Refactoring isValidString in Employee"
  * git tag -a v1.2.0 -m "version 1.2.0"
  * git push origin v1.2.0
* **Step14 :** Delete the previous tag (v1.2.0) due to a mistake (readme.md is missing):
  * git tag -d v1.2.0
  * git push origin --delete v1.2.0
* **Step15 :** Add a new tag (the right one - tag v1.1.3):
  * git tag -a v1.1.3 -m "version 1.1.3"
  * git push origin v1.1.3
* **Step16 :** Debug client side (react developer tools) and server side (Intellij)
* **Step17 :** Readme.md update, commit changes and tag this commit with tag v1.2.0
  * See step 11 for the commands to commit changes. The commit message for this commit is "New feature added and tested" 
  * See step 12 for the commands to tag the commit. 
* **Step18 :**  Modified the readme.md file, commit changes and tag the commit with tag ca1-part1.
  * See step 17 for the commands

#### 2. Steps and commands - part II
* **Step1 :** Create a branch called email-field:
  * To create the branch "email-field": git branch email-field
  * To see all the branches (local and remote):  git branch -a
  * To work on the new branch: git checkout email-field
  * To push the new branch to remote repository and, with the option -u, set the default remote branch for the current local branch: git push -u origin email-field
* **Step2 :** Create issue #3 to implement new feature (directly in Bitbucket)
* **Step3 :** Implement the new feature and tests for it
* **Step4 :** Debug client side (react developer tools) and server side (Intellij)
* **Step5 :** Commit the changes and resolve issue #3:
  * git status
  * git add .
  * git commit -m "Added new field email (resolving #3)"
  * git push
* **Step6 :** Update readme.md file and commit changes (to email-field branch). See step 5 for the git commands
* **Step7 :** Merge the email-field branch to master branch:
  * git checkout master
  * git merge email-field
  * git push 
* **Step8 :** Tag the last commit (9523d63):
  * git tag -a v1.3.0 -m "version 1.3.0"
  * git push origin v1.3.0
* **Step9 :** Create a branch called fix-invalid-email:
  * git branch fix-invalid-email
  * git checkout fix-invalid-email
  * git push -u origin fix-invalid-email
* **Step10 :** Create issue #4 to fix the email feature - Add validation the email field (directly in Bitbucket) 
* **Step11 :** Implement the email validation in the code and debug client side (react developer tools) and server side (Intellij)
* **Step12 :** Commit changes to branch fix-invalid-email:
  * git status
  * git add .
  * git commit -m "Added validation for field email (resolving #4)"
  * git push
* **Step13 :** Merge the fix-invalid-email branch to master branch:
  * git checkout master
  * git merge fix-invalid-email
  * git push
* **Step14 :** Commit changes made in readme.md file, master branch (see step 12 for the commands)
* **Step15 :** Tag the commit with tag v1.3.1 (see step 8 for the commands)
* **Step16 :** Delete the branch "fix-invalid-email" both locally and remotely:
  * git branch -d fix-invalid-email (locally)
  * git push origin --delete fix-invalid-email (remotely)
* **Step17 :** Create new branch "conflict" to purposely generate a conflict with master branch
  * git branch conflict
  * git checkout conflict
  * git push -u origin conflict
* **Step18 :** Add a comment on the code, class Employee, in line 39
* **Step19 :** Commit the changes (see step 12 for the commands)
* **Step20 :** Switch to master branch
  * git checkout master
* **Step21 :** Add a different comment on the code in the previous altered line (line 39 of class Employee)
* **Step22 :** Commit the changes (see step 12 for the commands)
* **Step23 :** Merge conflict branch into master branch (see step 13 for the commands; this time the "git checkout master" is not needed because I'm already in master branch)
* **Step24 :** Solve the conflict and finish the merge:
  * Delete the wrong comment and leave only the right one: (this is the desired comment: //Comment to generate a conflict)
  * git add .
  * git commit
  * git push
* **Step25 :** Delete the branch "conflict" both locally and remotely (see step 16 for the commands)
* **Step26 :** Update readme.md file, commit changes (see step 12) and tag the last commit with the tag ca1-part2 (see step 8)

#### 3. Analyse an alternative technological solution for version control (i.e., not based on Git)
* **Selected alternative: Mercurial and Mercurial Hosting with Helix TeamHub**

* **Advantages Helix TeamHub**
  * It is free for up to 5 users and 1GB of data storage (for academic purposes it should be enough).
  * Using this may give some intel on if it's worthy to pay for it later in the future.
  * Allows issues like bitbucket

* **Main differences between Git and Mercurial**
  * Git’s branching model is more effective
    * In Git, branches are only references to a certain commit
    * Git allows to delete branches, mercurial only allows to close branches
  * Git is supported in most know remote repositories while mercurial is not (GitHub and Bitbucket no longer support mercurial)
  * Many say that mercurial is more user-friendly
  * Git offers several commands for more specialized operations.

* **Git vs Mercurial**

| Subject for comparison | Git   | Mercurial |
|------------------- |-------------|------------------|
| Implementation |   Git can take longer for teams to ramp up given its higher level of complexity in commands and repository structure | Simpler and more intuitive commands can help teams to quickly ramp up adoption                  |
| Branching |   A branch is a pointer to a commit (SHA)             | A branch is embedded in a commit; branches cannot be renamed or deleted                  |
| History |   Mutable through rollback, cherry-pick, rebase             | Immutable beyond rollback                |
| Revision Tracking |   Each revision unique by calculated SHA-1              | Incremental, numerical index of revision (0, 1, 2, etc)                  |
| Rollback |   Supported via revert command; arbitrary via rebase and cherry-pick              | Supported via backout, revert commands                  |
| Community |   Git reports 5418 users and 1940 contributors on OpenHub 1730 jobs are returned by itjobswatch for Git, not counting other derivations like GitOps, GitHub, GitLab, etc.            | Mercurial reports 979 users and 728 contributors on OpenHub 44 jobs mention Mercurial on itjobswatch                |


* **Main commands for git and mercurial**

| Purpose of command | Git command | Mercurial command|
|------------------- |-------------|------------------|
| Start a repository |   git init              | hg init                  |
| Add contents to stage |  git add <. or file name>        | hg add <. or file name>
| Commit changes in stage |  git commit -m "commit message"    | hg commit -m "commit message"                         | 
| Push to remote               | git push       | hg push
| See changes to commit	 |     git status                                        |     hg status                            
| Create a branch locally		 | git branch <branchName>                   | hg branch <branchName>
| Push a branch to remote repository     		 | git push -u origin <branchName>              | hg push --new-branch
| Switch to a branch      		 | git checkout <branchName>              | hg update <branchName>
| Create a tag      		 | git tag -a <tagName> -m "message"              | hg tag <tagName>
| Merge branches     		 | git merge <branchName>     | hg merge <branchName>
| Pull from remote    		 | git pull   | hg pull


#### 4. Implementation of the alternative (Mercurial)
* **Step1 :** Create a repository in HelixTeamHub with Mercurial as VCS (devops-21-22-lmn-1211781_Mercurial)
* **Step2 :** Copy the local git repository to a new local folder (devops-21-22-lmn-1211781_Mercurial)
* **Step3 :** Delete the .git folder (and it's contents) and .gitignore file of the new folder
* **Step4 :** Change the configuration by creating file Mercurial.ini in %USERPROFILE% and add this content to the file:
  * [ui] 
    * username = Mariana Pereira <1211781@isep.ipp.pt>
  * [auth]
    * repo.prefix = https://1211781isepipppt@helixteamhub.cloud/skinny-brook-712/projects/devops-21-22-lmn-1211781/repositories/mercurial/devops-21-22-lmn-1211781_Mercurial
    * repo.username = 1211781isepipppt
    * repo.password = MyPassword
* **Step5 :** Make the new folder a Mercurial repository, commit changes and push them to the remote repository:
  * hg init
  * hg add .
  * hg commit -m "Initial commit"
  * hg push https://1211781isepipppt@helixteamhub.cloud/skinny-brook-712/projects/devops-21-22-lmn-1211781/repositories/mercurial/devops-21-22-lmn-1211781
* **Step6 :** Change the Mercurial.ini file to set default remote repository by adding this content:
  * [paths]
    * default = https://1211781isepipppt@helixteamhub.cloud/skinny-brook-712/projects/devops-21-22-lmn-1211781/repositories/mercurial/devops-21-22-lmn-1211781_Mercurial
* **Step7 :** Tag the initial version as v1.1.1 and push it to the server:
  * hg tag v1.1.1
  * hg push
* **Step8 :** Make some changes (delete the .gitignore that by mistake wasn't deleted) and commit the changes:
  * hg status (to confirm the changes)
  * hg add .
  * hg commit -m ".gitignore deleted"
  * hg push
* **Step9 :** Tag the commit as v1.2.0 and push it to the server (see step 7 for the commands)
* **Step10 :** Change the readme.md file, commit changes (see step 8 for the commands) and add the tag ca1-part1 (see step 7 for the commands)
* **Step11 :** Create a branch called email-field, make some changes in readme file and push it to remote repository:
  * hg branch email-field
  * hg add .
  * hg commit -m "Update readme file"
  * hg push --new-branch
* **Step12 :** Make some changes in readme.md file and commit the changes (see step 8 for the commands)
* **Step13 :** Tag the commit as v1.3.0 and push it to the server (see step 7 for the commands)
* **Step14 :** Switch to the default branch, make some changes in readme.md file and commit them:
  * hg update default
  * hg branch (to confirm that now I'm on default branch)
  * hg add .
  * hg commit -m "Update readme file" (this created a conflict)
  * hg merge email-field (did not work due to the conflict)
  * hg help resolve (to see how to use the hg resolve command)
  * hg resolve -la (to see the conflict): Accidentally changed the .idea\workspace.xml.
  * hg resolve -t internal:local .idea\workspace.xml: To choose my local version and resolve the conflict - did not work
  * Open the file in nano:  nano .idea/workspace.xml. Exit nano.
  * hg resolve -m .idea/workspace.xml 
  * hg resolve -la (conflict was solved - this command shows the letter R for resolved. Before was U for unresolved)
* **Step15 :** After solving the conflict commit the changes:
  * hg commit -m "Update readme file"
  * hg push
* **Step16 :** Try to merge the email-field branch into default branch:
  * hg merge email-field (not worked, it's necessary to commit the new changes in default branch)
* **Step17 :** Commit the changes (did not work, 1 file was deleted and that is the change to commit):
  * hg status
  * hg add .
  * hg commit -m "New commit"
  * hg push
* **Step18 :** Commit the deleted file:
  * hg addremove
  * hg add .
  * hg commit -m "New commit"
  * hg push 
* **Step19 :** Merge the email-field branch into default branch:
  * hg merge email-field (new conflict, this time in readme file and, for some reason, in workspace.xml again )
  * hg resolve -t internal:local CA1/readme.md
  * hg resolve -t internal:local .idea/workspace.xml (this time worked, before it failed because of the \)
  * hg add . (the next 3 commands were necessary because there were uncommitted changes)
  * hg commit -m "Other commit"
  * hg push
  * hg merge email-field
* **Step20 :** Tag the last commit with tag ca1-part2 (see step 7 for the commands)
* **Step20 :** Delete the branch email-field (with mercurial it's not possible to delete the branch since the VCS records the changes and not the files, it can be closed):
  * hg commit --close-branch

Remote repository: https://helixteamhub.cloud/skinny-brook-712/projects/devops-21-22-lmn-1211781/repositories/devops-21-22-lmn-1211781_Mercurial/tree/default