#### Assignment 4 : CA4

#### 1. Analyse an alternative (in this case Kubernetes - See https://kubernetes.io)
* **1.1 Present how the alternative tool compares to Docker regarding containerization features**

* **What is Docker?**
  * Docker is more than containers. It is a suite of tools for developers to build, share, run and orchestrate containerized apps.
  * Developer tools for building container images: Docker Build creates a container image, the blueprint for a container, including everything needed to run an application – the application code, binaries, scripts, dependencies, configuration, environment variables, and so on. Docker Compose is a tool for defining and running multi-container applications. These tools integrate tightly with code repositories (such as GitHub) and continuous integration and continuous delivery (CI/CD) pipeline tools (such as Jenkins).
  * Sharing images: Docker Hub is a registry service provided by Docker for finding and sharing container images with your team or the public. Docker Hub is similar in functionality to GitHub.
  * Running containers: Docker Engine is a container runtime that runs in almost any environment: Mac and Windows PCs, Linux and Windows servers, the cloud, and on edge devices. Docker Engine is built on top containerd, the leading open-source container runtime, a project of the Cloud Native Computing Foundation (DNCF).
  * Built-in container orchestration: Docker Swarm manages a cluster of Docker Engines (typically on different nodes) called a swarm. Here the overlap with Kubernetes begins.

* **What is Kubernetes?**
  * Kubernetes is an open-source container orchestration platform for managing, automating, and scaling containerized applications. Although Docker Swarm is also an orchestration tool, Kubernetes is the de facto standard for container orchestration because of its greater flexibility and capacity to scale.
  * Organizations use Kubernetes to automate the deployment and management of containerized applications. Rather than individually managing each container in a cluster, a DevOps team can instead tell Kubernetes how to allocate the necessary resources in advance.
  * Where Kubernetes and the Docker suite intersect is at container orchestration. So when people talk about Kubernetes vs. Docker, what they really mean is Kubernetes vs. Docker Swarm.

* **What are the challenges of container orchestration?**
  * Although Docker Swarm and Kubernetes both approach container orchestration a little differently, they face the same challenges. A modern application can consist of dozens to hundreds of containerized microservices that need to work together smoothly. They run on multiple host machines, called nodes. Connected nodes are known as a cluster.
  * There must be number of mechanisms in place to coordinate such a distributed system. These mechanisms are often compared to a conductor directing an orchestra to perform elaborate symphonies and juicy operas for our enjoyment.
  * Here are some of the tasks orchestration platforms are challenged to perform:
    * **Container deployment:** In the simplest terms, this means to retrieve a container image from the repository and deploy it on a node. However, an orchestration platform does much more than this: it enables automatic re-creation of failed containers, rolling deployments to avoid downtime for the end-users, as well as managing the entire container lifecycle.
    * **Scaling:** This is one of the most important tasks an orchestration platform performs. The “scheduler” determines the placement of new containers so compute resources are used most efficiently. Containers can be replicated or deleted on the fly to meet varying end-user traffic.
    * **Networking:** The containerized services need to find and talk to each other in a secure manner, which isn’t a trivial task given the dynamic nature of containers. In addition, some services, like the front-end, need to be exposed to end-users, and a load balancer is required to distribute traffic across multiple nodes.
    * **Observability:** An orchestration platform needs to expose data about its internal states and activities in the form of logs, events, metrics, or transaction traces. This is essential for operators to understand the health and behavior of the container infrastructure as well as the applications running in it.
    * **Security:** Security is a growing area of concern for managing containers. An orchestration platform has various mechanisms built in to prevent vulnerabilities such as secure container deployment pipelines, encrypted network traffic, secret stores and more. However, these mechanisms alone are not sufficient, but require a comprehensive DevSecOps approach.

* **Kubernetes vs Docker Swarm**
  * Both Docker Swarm and Kubernetes are production-grade container orchestration platforms, although they have different strengths.
  * Docker Swarm, also referred to as Docker in swarm mode, is the easiest orchestrator to deploy and manage. It can be a good choice for an organization just getting started with using containers in production. Swarm solidly covers 80% of all use cases with 20% of Kubernetes’ complexity.
  * Swarm seamlessly integrates with the rest of the Docker tool suite, such as Docker Compose and Docker CLI, providing a familiar user experience with a flat learning curve. As you would expect from a Docker tool, Swarm runs anywhere Docker does and it’s considered secure by default and easier to troubleshoot than Kubernetes.
  * Kubernetes, or K8s for short, is the orchestration platform of choice for 88% of organizations. Initially developed by Google, it’s now available in many distributions and widely supported by all public cloud vendors. Amazon Elastic Kubernetes Service, Microsoft Azure Kubernetes Service, and Google Kubernetes Platform each offer their own managed Kubernetes service. Other popular distributions include Red Hat OpenShift, Rancher/SUSE, VMWare Tanzu, IBM Cloud Kubernetes Services. Such broad support avoids vendor lock-in and allows DevOps teams to focus on their own product rather than struggling with infrastructure idiosyncrasies.
  * The true power of Kubernetes comes with its almost limitless scalability, configurability, and rich technology ecosystem including many open-source frameworks for monitoring, management, and security.

| Kubernetes | Docker Swarm   | 	
|------------------- |-------------|
| Complex installation | Easier installation | 
| More complex with a steep learning curve, but more powerful |  Lightweight and easier to learn but limited functionality | 
| Supports auto-scaling | Manual scaling | 
| Built-in monitoring | Needs third party tools for monitoring | 
| Manual setup of load balancer  | Auto load balancer   | 
| Need for separate CLI tool |  Integrated with Docker CLI	 |       

* **Docker and Kubernetes**
  * Simply put, the Docker suite and Kubernetes are technologies with different scopes. You can use Docker without Kubernetes and vice versa, however they work well together.
  * From the perspective of a software development cycle, Docker’s home turf is development. This includes configuring, building, and distributing containers using CI/CD pipelines and DockerHub as an image registry. On the other hand, Kubernetes shines in operations, allowing you to use your existing Docker containers while tackling the complexities of deployment, networking, scaling, and monitoring.
  * Although Docker Swarm is an alternative in this domain, Kubernetes is the best choice when it comes to orchestrating large distributed applications with hundreds of connected microservices including databases, secrets and external dependencies.

  
* **1.2 Describe how the alternative tool could be used to solve the same goals as presented for this assignment**
* **Using Docker desktop** *
* **Step1 :** Kubernetes setup:
  * Kubernetes can be enabled from the Kubernetes settings panel: Go to docker desktop -> settings -> Kubernetes -> Enable Kubernetes -> Apply & Restart : triggers the installation of a single-node Kubernetes cluster.
  * Internally, the following actions are triggered in the Docker Desktop Backend and VM:
    * Generation of certificates and cluster configuration
    * Download and installation of Kubernetes internal components
    * Cluster bootup
    * Installation of additional controllers for networking and storage
  * To get the services (service is an abstraction which represents a logical set of Pods and a policy by which to access them): kubectl get services
  * To check the system pods: kubectl get pods -n kube-system
* **Step2 :** Install Kompose:
  * Linux: 
    * curl -L https://github.com/kubernetes/kompose/releases/download/v1.26.1/kompose-linux-amd64 -o kompose
    * chmod +x kompose
    * sudo mv ./kompose /usr/local/bin/kompose
* **Step3 :** Create Kubernetes Manifests with Kompose:
  * To convert the docker-compose.yml file to files that you can use with kubectl, go to the directory containing your docker-compose.yml and run:
    * kompose convert -f <docker compose file name>  : in my case, the name is: docker-compose.yml
* **Step4 :** Deploy Resources to Kubernetes Cluster:
  * kubectl apply -f <output file> : specifying all the files created by Kompose or use . instead of the output file to use all files generated by kompose
