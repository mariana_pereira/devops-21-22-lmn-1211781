#### Assignment 4 : CA4

#### 1. Steps and commands - part II
* **Step1 :** Create folder CA4_part2:
  * mkdir /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA4/CA4_part2
* **Step2 :** Create file readme:
  * touch /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA4/CA4_part2/readme.md
* **Step3 :** Copy the application folder (react-and-spring-data-rest-basic) form CA2/CA2_part2 to CA4_part2
* **Step4 :** Create the Dockerfiles for both the web container (this container is used to run Tomcat and the spring application) and
* the db container (this container is used to execute the H2 server database)
* **Step5 :** Create the docker-compose.yml file
* **Step6 :** Create a backup folder for the database on the host machine:
  *  mkdir data
* **Step7 :** Update the IP in application.properties in line 7
* **Step8 :** Create both folders web and db in the root of the project and put inside the respective dockerfile
* **Step9 :** Create issue in Bitbucket
* **Step10 :** Commit the changes:
  * git add .
  * git commit -m "Create dockerfiles and docker-compose (resolving #19)"
  * git push
* **Step11 :** Build the images:
  * docker-compose up (in the folder where the docker-compose.yml file is)
  * an error occurred: forgot to start docker desktop
  * run docker-compose up again
* **Step12 :** Verify if everything is working:
  * In the host you can open the spring web application using the following url:
    * http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT
  * To open the H2 console use the following url:
    * http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console
    * for the connection string use: jdbc:h2:tcp://192.168.56.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
    * it's possible, using sql, to make CRUD operations in the db
* **Step13 :** Tag the images and publish it in docker hub (ca4_part2_db and ca4_part2_web):
  * docker login
  * docker tag ca4_part2_db devopsmariana/ca4_part2_db:1.0
  * docker push devopsmariana/ca4_part2_db:1.0
  * docker tag ca4_part2_web devopsmariana/ca4_part2_web:1.0
  * docker push devopsmariana/ca4_part2_web:1.0
* **Step14 :**  Use a volume with the db container to get a copy of the database file by using the exec to run a shell in the container and copying the database file to the volume:
  * List the containers: docker ps or docker container ls (in this case will use the name and not the id)
  * Execute a command to enter service/container: docker-compose exec db bash
  * cp jpadb.mv.db /usr/src/data-backup
  * To leave the container type: exit
* **Step15 :** Commit the changes and tag the commit with tag ca4-part2:
  * git add .
  * git commit -m "Assignment ca4-part2"
  * git push
  * git tag -a ca4-part2 -m "version ca4-part2"
  * git push origin ca4-part2



