# A basic apache server.
FROM httpd:2.4
# Install java
RUN apt-get update
RUN apt-get install openjdk-11-jdk-headless -y
# Install git
RUN apt-get install git -y
# Change the working directory (due to the error exposed in the readme file in step 13)
WORKDIR /temp-build
# Clone the repository into the docker container
RUN git clone https://Mariana_Pereira@bitbucket.org/mariana_pereira/devops-21-22-lmn-1211781.git
# Go to the root of the project
WORKDIR /temp-build/devops-21-22-lmn-1211781/CA4/CA4_part1/gradle_basic_demo
# Expose port (update done in step 14)
EXPOSE 59001

# Give the required permissions to execute the application
RUN chmod u+x gradlew
# Run the clean build to build the project - this time inside the container - removing the build folder (if exists) thus cleaning everything including leftovers from previous builds which are no longer relevant.
RUN ./gradlew clean build
# Copy the jar file to be able to execute the app (after this step the clean task will be executed so the build folder will be deleted)
RUN cp build/libs/basic_demo-0.1.0.jar .
# Run the clean task to clean the build
RUN ./gradlew clean
# To run the server
CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

# Copy files under ./public-html on the host to /usr/local/apache2/htdocs/ on the container
# COPY ./public-html/ /usr/local/apache2/htdocs/
