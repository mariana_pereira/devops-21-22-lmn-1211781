#### Assignment 4 : CA4

#### 1. Steps and commands - part I
* **Step1 :** Create folder CA4:
  * mkdir /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA4
* **Step2 :** Create folder CA4_part1:
  * mkdir /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA4/CA4_part1
* **Step3 :** Create file readme:
  * touch /c/WsSwitchISEP/devops-21-22-lmn-1211781/CA4/CA4_part1/readme.md
* **Step4 :** Download and install Docker Desktop for Windows from:
  * https://docs.docker.com/desktop/windows/install/
* **Step5 :** Clone into CA4/CA4_part1 the repository that is the target of this assignment:
  * git clone https://bitbucket.org/luisnogueira/gradle_basic_demo.git
* **Step6 :** Remove the .git folder (and it's contents) and the .gitignore file (from folder CA4/CA4_part1/gradle_basic_demo)
* **Step7 :** Create an empty dockerfile in folder CA4_part1 (named "Dockerfile") for the 1º version of this assignment
* **Step8 :** Update the dockerfile (line 2) to build containers (docker images) from the image:
  * httpd (The Apache HTTP Server)
* **Step9 :** Update the dockerfile to create a docker image that will execute the chat server:
  * In this version of the dockerfile the  chat server will be built  "inside" the Dockerfile
  * See the file "CA4/CA4_part1/Dockerfile" -> each line has a comment (above the command) that identifies the purpose of the command
* **Step10 :** In the folder where the Dockerfile is (...CA4/CA4_part1) run:
  * docker build -t img_ca4_part1_v1 . 
  * Got an error because git wasn't installed, add the line to install git and java in the container to the Dockerfile
* **Step11 :** Create issue in Bitbucket to create the first Dockerfile (for the 1º version of this assignment - build the chat server "inside" the Dockerfile)
* **Step12 :** Commit the changes, resolving the issue from step 11
  * git add .
  * git commit -m "Create the first Dockerfile (resolving #17)"
  * git push
* **Step13 :** In the folder where the Dockerfile is (...CA4/CA4_part1) run:
  * docker build -t img_ca4_part1_v1 . -> Got another error, update the dockerfile, WORKDIR command updated and another WORKDIR command line added
  * run: docker build -t img_ca4_part1_v1 .  -> Got an error, update the .gitignore file (in the repository) adding the line: !gradle/wrapper/gradle-wrapper.jar
  * commit the changes (see step 12 for the commands) and the gradle-wrapper.jar file (git add -f gradle/wrapper/gradle-wrapper.jar)
* **Step14 :** Update the Dockerfile (to fix some mistakes) and run (in the folder of the dockerfile): 
  * docker build -t img_ca4_part1_v1 . (to build the image)
  * docker images (to make sure the image was created)
  * docker run 59001:59001 -p img_ca4_part1_v1 (to run the image - with the option -d it would run in the background)
* **Step15 :** Test the application:
  * In the root of the project (devops-21-22-lmn-1211781/CA4/CA4_part1/gradle_basic_demo) run: ./gradlew runClient (in to different terminals)
  * The application works fine, is possible to communicate between clients
  * Run: docker container ls -a (to see the id of the container) 
  * docker stop 1aed98612b8b (to stop the container)
* **Step16 :** Commit the changes (see step 12 for the commands)
* **Step17 :** Create a new dockerfile (v2.Dockerfile) for the 2º version of this assignment and rename the first to: v1.Dockerfile
* **Step18 :** Create an issue in Bitbucket to create the dockerfile for the 2º version (#18)
* **Step19 :** Commit the changes (see step 12 for the commands)
* **Step20 :** Update the v2.Dockerfile (and also v1.Dockerfile due to some improvements made) and commit the changes (see step 12 for the commands)
* **Step21 :** Build the app in the host and create the new image from v2.Dockerfile:
  * In the root of the project (devops-21-22-lmn-1211781/CA4/CA4_part1/gradle_basic_demo) run: ./gradlew clean build
  * In the folder of the v2.Dockerfile file run: 
    * docker build -f v2.Dockerfile -t img_ca4_part1_v2 . (to build the image)
    * docker images (to make sure the image was created)
    * docker run 59001:59001 -p img_ca4_part1_v2 (to run the container - with the option -d it would run in the background) -> Got an error: "unable to find image locally"
    * docker run --help (realized the previous command wasn't correct)
    * docker run -p 59001:59001 -d img_ca4_part1_v2 (this time worked)
* **Step22 :** Test the application:
  * In the root of the project (devops-21-22-lmn-1211781/CA4/CA4_part1/gradle_basic_demo) run: ./gradlew runClient (in to different terminals)
  * The application works fine, is possible to communicate between clients
  * Run: docker container ls -a (to see the id of the container)
  * docker stop 6174f1bb47f8 (to stop the container)
* **Step23 :** Commit the changes (see step 12 for the commands) 
* **Step24 :** Recreate an image from v1.Dockerfile (to test the changes made in step 20):
  * In the folder of the v1.Dockerfile file run:
    * docker build -f v1.Dockerfile -t img_ca4_part1_v1_2 . (to build the image)
    * docker images (to make sure the image was created)
    * docker run -p 59001:59001 -d img_ca4_part1_v1_2 (to run the container)
  * Test the application:
    * In the root of the project (devops-21-22-lmn-1211781/CA4/CA4_part1/gradle_basic_demo) run: ./gradlew runClient (in to different terminals)
    * The application works fine, is possible to communicate between clients
    * docker container ls -a (to see the id of the container)
    * docker stop c1496fc2c75f (to stop the container)
* **Step25 :** Commit the changes (see step 12 for the commands)
* **Step26 :** Create a public repository in dockerhub named devops (realized after that this step wasn't necessary)
  * link: https://hub.docker.com/repository/docker/devopsmariana/devops
* **Step27 :** Tag the images and publish it in docker hub (img_ca4_part1_v1_2 and img_ca4_part1_v2):
  * First image:
    * docker login 
    * docker tag img_ca4_part1_v1_2 devopsmariana/img_ca4_part1_v1_2:1.1
    * docker push devopsmariana/img_ca4_part1_v1_2:1.1
  * Second image:
    * docker login
    * docker tag img_ca4_part1_v2 devopsmariana/img_ca4_part1_v2:1.0
    * docker push devopsmariana/img_ca4_part1_v2:1.0
* **Step28 :** Commit the changes, resolving issue 18, and tag the commit with tag ca4-part1:
  * git add .
  * git commit -m "Assignment ca4-part1 (resolving #18)"
  * git push
  * git tag -a ca4-part1 -m "version ca4-part1"
  * git push origin ca4-part1
