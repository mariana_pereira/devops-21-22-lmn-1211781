# A basic apache server.
FROM httpd:2.4
# Install java
RUN apt-get update
RUN apt-get install openjdk-11-jdk-headless -y

# Change the working directory (due to the error exposed in the readme file in step 13)
WORKDIR /temp-build
# Expose port
EXPOSE 59001

# Copy the jar file into the container to be able to run the app
COPY gradle_basic_demo/build/libs/basic_demo-0.1.0.jar .

# To run the server
CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

# Copy files under ./public-html on the host to /usr/local/apache2/htdocs/ on the container
# COPY ./public-html/ /usr/local/apache2/htdocs/
